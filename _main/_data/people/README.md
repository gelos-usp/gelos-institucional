# Membros do GELOS

Adicione um arquivo .yml para incluir um novo membro. O nome do arquivo é o identificador do usuário (apelido, etc, ascii e tudo minúsculo), segue um formato YAML:

```yaml
name: Fulano da Silva
# Apenas name é obrigatório, tudo abaixo disso é opcional
# Fique a vontade para criar campos novos!
nusp: 12345678
gitlab: fulano
email: fulano@usp.com
site: fulano.com.br
```

Você pode fazer isso para você mesmo, e fica ao seu critério quais dados além
de `name` colocar. Você pode adicionar outros membros caso precise incluir num
relatório, nesse caso coloque apenas o nome/pseudônimo (consulte a pessoa pra
ver qual ela prefere).

Você pode editar aqui pela interface do gitlab, ou clonar o repositório usando
`git`. Nos dois casos, abra um MR e um mantenedor irá aprovar logo mais.
